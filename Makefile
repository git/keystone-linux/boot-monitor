# /*
#  * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/
#  *
#  *  Redistribution and use in source and binary forms, with or without
#  *  modification, are permitted provided that the following conditions
#  *  are met:
#  *
#  *    Redistributions of source code must retain the above copyright
#  *    notice, this list of conditions and the following disclaimer.
#  *
#  *    Redistributions in binary form must reproduce the above copyright
#  *    notice, this list of conditions and the following disclaimer in the
#  *    documentation and/or other materials provided with the
#  *    distribution.
#  *
#  *    Neither the name of Texas Instruments Incorporated nor the names of
#  *    its contributors may be used to endorse or promote products derived
#  *    from this software without specific prior written permission.
#  *
#  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#  *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
#  *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#  *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#  *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#  *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#  *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
#  *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#  *
#  */
include makedefs

NONSECPADBIN = non-sec/nonsec_pad.bin
SECPADBIN = sec/s-kernel_pad.bin
IMAGE = skern
NONSECLDS = non-sec/nonsec.lds
SECLDS = sec/s-kernel.lds
NONSECLDS2 = non-sec/nonsec2.lds
SECLDS2 = sec/s-kernel2.lds


BOOTROM_VERSION = 1

.PHONY: all k2hk k2l k2e build_clean

all:
	$(MAKE) k2hk
	$(MAKE) k2l
	$(MAKE) k2e

k2hk:
	@if [ -f $(IMAGE).bin ] ; \
	then \
		make build_clean; \
	fi

	@echo "#define IMAGE_LOAD_ADDR ${IMAGE_LOAD_ADDR_K2HK}" >>config.h
	@echo "#define RBL_MON_STACK_START ${RBL_MON_STACK_START_K2HK}" >>config.h
	@echo "#define ARM_NUM_CORES ${ARM_NUM_CORES_K2HK}" >>config.h

	make copy_common_headers
	make build_image
	cp $(IMAGE).bin $(IMAGE)-k2hk.bin

k2e:
	@if [ -f $(IMAGE).bin ] ; \
	then \
		make build_clean; \
	fi

	@echo "#define IMAGE_LOAD_ADDR ${IMAGE_LOAD_ADDR_K2LE}" >>config.h
	@echo "#define RBL_MON_STACK_START ${RBL_MON_STACK_START_K2E}" >>config.h
	@echo "#define ARM_NUM_CORES ${ARM_NUM_CORES_K2E}" >>config.h

	cp $(NONSECLDS) $(NONSECLDS2)
	cp $(SECLDS) $(SECLDS2)
	sed -i 's/${IMAGE_LOAD_ADDR_K2HK}/${IMAGE_LOAD_ADDR_K2LE}/g' $(NONSECLDS)
	sed -i 's/${IMAGE_LOAD_ADDR_SEC_K2HK}/${IMAGE_LOAD_ADDR_SEC_K2LE}/g' $(SECLDS)

	make copy_common_headers
	make build_image
	cp $(IMAGE).bin $(IMAGE)-k2e.bin
	mv $(NONSECLDS2) $(NONSECLDS)
	mv $(SECLDS2) $(SECLDS)

k2l:
	@if [ -f $(IMAGE).bin ] ; \
	then \
		make build_clean; \
	fi

	@echo "#define IMAGE_LOAD_ADDR ${IMAGE_LOAD_ADDR_K2LE}" >>config.h
	@echo "#define RBL_MON_STACK_START ${RBL_MON_STACK_START_K2L}" >>config.h
	@echo "#define ARM_NUM_CORES ${ARM_NUM_CORES_K2L}" >>config.h

	cp $(NONSECLDS) $(NONSECLDS2)
	cp $(SECLDS) $(SECLDS2)
	sed -i 's/${IMAGE_LOAD_ADDR_K2HK}/${IMAGE_LOAD_ADDR_K2LE}/g' $(NONSECLDS)
	sed -i 's/${IMAGE_LOAD_ADDR_SEC_K2HK}/${IMAGE_LOAD_ADDR_SEC_K2LE}/g' $(SECLDS)

	make copy_common_headers
	make build_image
	cp $(IMAGE).bin $(IMAGE)-k2l.bin
	mv $(NONSECLDS2) $(NONSECLDS)
	mv $(SECLDS2) $(SECLDS)

copy_common_headers:
	@echo "#define RBL_MON_STACK_SIZE 0x800" >>config.h
	@echo "#define ARM_ERRATA_763126 1" >>config.h
	@echo "#define ARM_ERRATA_798870 1" >>config.h
	@echo "#define ARM_ERRATA_799270 1" >>config.h
	@echo "#define NONSEC_IMAGE_SIZE ${NONSEC_IMAGE_SIZE}" >>config.h

build_image:
	(cd sec; make)
	(cd non-sec; make)
	cat $(NONSECPADBIN) >> $(IMAGE).bin
	cat $(SECPADBIN) >> $(IMAGE).bin

build_clean:
	rm -rf $(IMAGE).bin
	rm -f config.h
	cd non-sec; make clean;
	cd sec; make clean;

clean: build_clean
	rm -rf $(IMAGE)-k2hk.bin
	rm -rf $(IMAGE)-k2e.bin
	rm -rf $(IMAGE)-k2l.bin
