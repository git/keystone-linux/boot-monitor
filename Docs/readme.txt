The boot monitor is an application that provides secure previleged
services to Linux kernel to execute functions in secure privileged
mode. It is invoked through smc arm instruction. The skern flat binary
image is created by the build system that can be loaded to MSCM
through u-boot or CCS. boot monitor is installed and initialized through
install_skern u-boot command. It is to be loaded at MSMC address
0xc5f0000 and the same address is passed to the install_skern
command as arg1.

to build boot monitor
======================

Need linaro tool chain. Set your CROSS_COMPILE path as

export CROSS_COMPILE=arm-linux-gnueabihf-

Assume boot-monitor.git is cloned to your home directory.

To build boot monitor do

cd ~/boot-monitor
make clean
make

build will generate skern.bin under cwd. This can be loaded to MSMC SRAM
through CCS or u-boot
