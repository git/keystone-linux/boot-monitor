/*
 * Copyright (C) 2013-2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <errno.h>

#include "../config.h"
#include "skernel.h"

unsigned int debug = 0;

#define ARM_CLUSTER_NUM_CPUS	ARM_NUM_CORES

#define GIC_XXX			0x02561080
#define DPSC_BASE		0x01e80000
#define CORE_N_BASE_ADDR_ARM0	0x02501040

#define BIT(x)	(1 << (x))

/* DPSC  offsets */
#define TETRIS_PD_DPSC_OFFSET	0x400
#define TETRIS_PD_CPU_N_PTCMD	(0 >> 2)
#define TETRIS_PD_CPU_N_PDSTAT	(4 >> 2)
#define TETRIS_PD_CPU_N_PTCTL	(8 >> 2)

/* PD States */
#define	PD_ON			0
#define PD_ON_HELD_IN_RESET	1
#define	PD_OFF_WAKEUP_INTR	2
#define	PD_OFF			3

/* Masks */
#define PD_NEXT_STATE_MASK	(BIT(0) | BIT(1))
#define PD_ACTUAL_STATE_MASK	(BIT(16) | BIT(17) | BIT(18))
#define MACHINE_PSM_STATE_MASK	(BIT(27) | BIT(28) | BIT(29) | \
				 BIT(30) | BIT(31))
#define PD_ACTUAL_STATE_SHIFT	16
#define PD_PSM_STATE_SHIFT	27

#define GO_CPU			1

/* PCSI definitions */
#define PSCI_VERSIONS		0x84000000
#define PSCI_SUSPEND		0x84000001
#define PSCI_CPU_OFF		0x84000002
#define PSCI_CPU_ON		0x84000003
#define PSCI_AFINITY_INFO	0x84000004
#define PSCI_MIGRATE		0x84000005
#define PSCI_MIGRATE_INFO_TYPE	0x84000006
#define PSCI_INFO_UP_CPU	0x84000007
#define PSCI_SYS_OFF		0x84000008
#define PSCI_SYS_RESET		0x84000009

#define PSCI_SUCCESS		0
#define PSCI_NOT_SUPPORTED	-1
#define PSCI_INV_PARAM		-2
#define PSCI_DENIED		-3
#define PSCI_ALREADY_ON		-4
#define PSCI_ON_PENDING		-5
#define PSCI_INTERNAL_FAILURE	-6
#define PSCI_NOT_PRESENT	-7
#define PSCI_DISABLED		-8

/*
 * MPU settings to allow user space access to BOOTCFG and PSC
 * registers.
 */
#define MPU_CFG_VAL		0x03fffcb6
#define MPU_CFG_REG1		0x02389a48
#define MPU_CFG_REG2		0x02389ac8

/* MPU settings for User mode access for MPU 1, 2 and 5 */
#define USER_MODE_CFG_VAL	0x3fffcb6
#define MPU_CFG_REG3		0x02368208
#define MPU_CFG_REG4		0x02370208
#define MPU_CFG_REG5		0x02388208

struct cpu_data {
	void *kern_ep;
	short boot_complete;
};

struct skern_cpu_data {
	struct cpu_data cpu[ARM_CLUSTER_NUM_CPUS];
	/* physical address */
	unsigned int tetris_dpsc_base;
	/* frequency updated in CNTFREQ of ARM Generic timer */
	unsigned int arch_timer_freq;
};

#ifdef SIMULATOR
/* work around for secondary core power up */
unsigned int *ptr = (unsigned int *)0x800001f0;
#endif

static struct skern_cpu_data skernel_cpu_data;

struct regs {
	unsigned	psr;
	unsigned	r0;
	unsigned	r1;
	unsigned	r2;
	unsigned	r3;
	unsigned	r4;
	unsigned	r5;
	unsigned	r6;
	unsigned	r7;
	unsigned	r8;
	unsigned	r9;
	unsigned	r10;
	unsigned	r11;
	unsigned	r12;
	unsigned	lr;
};

#ifdef DEBUG
int mpu_dump[64];
#endif

int skern_command(int cmd, const struct regs *regs)
{
	unsigned int mpu_reg;
	int error, i;

	if ((regs->psr & 0x1f) == 0x10) {
		if (debug)
			skern_puts("Attempted SMC from user mode!\n\r");
		return PSCI_DENIED;
	}

	switch (cmd) {
	case 0:
	case PSCI_CPU_ON:
		error =  skern_poweron_cpu(regs->r1,		/* cpu    */
					   (void *)regs->r2);	/* entry  */
		break;
	case 1:
		error =  skern_poweroff_cpu(regs->r1);		/* cpu    */
		break;

#ifdef DEBUG
	case 2:
		mpu_reg = MPU_CFG_REG3;
		for (i = 0; i < 16; i++) {
			mpu_dump[i] = *(unsigned int *)mpu_reg;
			mpu_reg += 0x10;
		}
		mpu_reg = MPU_CFG_REG4;
		for (i = 16; i < 30; i++) {
			mpu_dump[i] = *(unsigned int *)mpu_reg;
			mpu_reg += 0x10;
		}
		mpu_reg = MPU_CFG_REG5;
		for (i = 30; i < 44; i++) {
			mpu_dump[i] = *(unsigned int *)mpu_reg;
			mpu_reg += 0x10;
		}
		mpu_dump[44]  = 0xdeadbeef;
		break;
#endif
	case PSCI_VERSIONS:
		return 0x00000002;

	case PSCI_SUSPEND:
		return PSCI_NOT_SUPPORTED;

	case PSCI_CPU_OFF:
		error =  skern_poweroff_cpu(chip_get_arm_num());
		skern_puts("OOPS, PSCI_CPU_OFF DIDN'T WORK\r\n");
		break;

	default:
		error = PSCI_NOT_SUPPORTED;
	}
	return error;
}

void skern_setup_l2_latency(void)
{
        asm volatile (
	/*  L2CTLR (w in ns 0) */
	"mrc    p15, 1, r0, c9, c0, 2\n"
	/* DRS      (2 S) <A1>Vslices: #2 */
	"orr    r0, r0, #(2 << 10)\n"
	/* TRL (2 c) <A1>V Tag ram latency : 2 cycles */
	"orr    r0, r0, #(1 << 6)\n"
	/* DRL (4 c) <A1>V Data Ram latency : 4cycles */
	"orr    r0, r0, #(3 << 0)\n"
	/* ECC and Parity enable for L1 and L2 Cache */
	"orr    r0, r0, #(1 << 21)\n"
	"mcr    p15, 1, r0, c9, c0, 2\n"
	"isb\n"
	"dsb\n"
	:
	:
	: "cc", "r0", "memory"
	);
}

void skern_setup_cp15(void)
{
        asm volatile (
	/* enable VFP and NEON for non-sec and sec */
	"mrc    p15, 0, r0, c1, c1, 2\n"
	"orr    r0, r0, #(3 << 10)\n"
	"bic    r0, r0, #(3 << 14)\n"
	"mcr    p15, 0, r0, c1, c1, 2\n"
	"isb\n"
	"dsb\n"
	/* disable SMP bit */
	"mrc	p15, 0, r0, c1, c0, 1\n"
	"bic	r0, r0, #(1 << 6)\n"
	"mcr	p15, 0, r0, c1, c0, 1\n"
	"isb\n"
	"dsb\n"
	/* set NSACR.NS_SMP bit */
	"mrc	p15, 0, r0, c1, c1, 2\n"
	"orr	r0, r0, #(1 << 18)\n"
	"mcr	p15, 0, r0, c1, c1, 2\n"
	"mov    r0, #0x00f00000\n"
	"mcr    p15, 0, r0, c1, c0, 2\n"
	"isb\n"
	"dsb\n"
	/* set L2ACTLR[.3] = 1 to workaround stream performance issue */
	"mrc    p15, 1, r0, c15, c0, 0\n"
	"orr    r0, r0, #(1 << 3)\n"
	"orr	r0, r0, #(1 << 8)\n"
	"mcr    p15, 1, r0, c15, c0, 0\n"
	"mov    r0, #0x40000000\n"
	/* vmsr   FPEXC, r0 */
	".inst  0xeee80a10\n"
	:
	:
	: "cc", "r0", "memory");
}

extern void _skern_123_init(void);

void *skern_init(unsigned int (*fcn_p()), unsigned int from,
				unsigned int dpsc_base, unsigned int freq)
{
	int i, cpu_id = chip_get_arm_num();
	unsigned int *addr = (unsigned int *)CORE_N_BASE_ADDR_ARM0;

	if (debug) {
		skern_puts("Message2 from Secure Mode\n\r");
		skern_puts("Core freq - 0x");
		skern_putui(freq);
		skern_puts("\n\r");
	}

	skernel_cpu_data.tetris_dpsc_base = DPSC_BASE;
	/* hard code the arch timer frquency now */
	skernel_cpu_data.arch_timer_freq = freq;

	for (i = 1; i < ARM_CLUSTER_NUM_CPUS; i++) {
		addr[i * 2] = (unsigned int )_skern_123_init;
	}

	/* initialize the GIC, registers to enable group1 irqs */
	addr = (unsigned int*)GIC_XXX;
	for (i = 1; i < 16; i++)
		addr[i] = 0;
	/*
	 * Enable user space access to BOOTCFG and PSC registers.
	 * Needed to enable user space boot up of DSPs.
	 * Currently this is a quick fix. Real solution is to
	 * add an SMC instruction that can be called from user
	 * space to enable power up/down of DSPs
	 */
	addr = (unsigned int *)MPU_CFG_REG1;
	*addr = MPU_CFG_VAL;
	addr = (unsigned int *)MPU_CFG_REG2;
	*addr = MPU_CFG_VAL;

	/* Set the Linux user space access MPUs */
	addr = (unsigned int *)MPU_CFG_REG3;
	for (i = 0; i < 16; i++) {
		*addr = USER_MODE_CFG_VAL;
		addr += 4;
	}
	addr = (unsigned int *)MPU_CFG_REG4;
	for (i = 0; i < 14; i++) {
		*addr = USER_MODE_CFG_VAL;
		addr += 4;
	}
	addr = (unsigned int *)MPU_CFG_REG5;
	for (i = 0; i < 14; i++) {
		*addr = USER_MODE_CFG_VAL;
		addr += 4;
	}

	/* setup L2 latency values */
	skern_setup_l2_latency();

	/* set up basic A15 features or configurations */
	skern_setup_cp15();

	/* set the CNTFREQ */
	asm volatile ("mcr	p15, 0, %0, c14, c0, 0"
	    : /* No output operands */
	    : "r" (skernel_cpu_data.arch_timer_freq));

	return skernel_cpu_data.cpu[cpu_id].kern_ep;
}

void *skern_123_init(void)
{
	int i, cpu_id = chip_get_arm_num();

	if (debug) {
		skern_puts("Message2 from Secure Mode\n\r");
		skern_puts("Core freq - 0x");
		skern_putui(skernel_cpu_data.arch_timer_freq);
		skern_puts("\n\r");
	}
	/* setup L2 latency values */
	skern_setup_l2_latency();

	/* set up basic A15 features or configurations */
	skern_setup_cp15();

	/* set the CNTFREQ */
	asm volatile ("mcr	p15, 0, %0, c14, c0, 0"
	    : /* No output operands */
	    : "r" (skernel_cpu_data.arch_timer_freq));

	return skernel_cpu_data.cpu[cpu_id].kern_ep;
}

/* get dspc register word offset */
static int skern_get_dpsc_offset(int cpu_id)
{
	int offset;

	offset = (cpu_id << 3) + (cpu_id << 2);
	offset >>= 2;
	return offset;
}

/* skern services below */
int skern_poweron_cpu(int cpu_id, void *kern_ep)
{
	unsigned int volatile *addr;
	unsigned int val;

	if (debug)
		skern_puts(">>>> skern_poweron_cpu >>>>\n\r");

	if (cpu_id >= ARM_CLUSTER_NUM_CPUS)
		return PSCI_INV_PARAM;

	if (!skernel_cpu_data.tetris_dpsc_base)
		return PSCI_INTERNAL_FAILURE;

	skernel_cpu_data.cpu[cpu_id].kern_ep = kern_ep;
	addr = (unsigned int *)(skernel_cpu_data.tetris_dpsc_base +
					TETRIS_PD_DPSC_OFFSET);

#ifdef SIMULATOR
	/* this is a temporary workaround to start secondary core */
	*(ptr + cpu_id) = 0;
#endif

	/* power on the DPSC */
	addr += skern_get_dpsc_offset(cpu_id);
	val = *(addr + TETRIS_PD_CPU_N_PDSTAT);
	if (((val & PD_ACTUAL_STATE_MASK) >> PD_ACTUAL_STATE_SHIFT) == PD_ON)
		return PSCI_ALREADY_ON;

	*(addr + TETRIS_PD_CPU_N_PTCTL) = PD_ON;
	*(addr + TETRIS_PD_CPU_N_PTCMD) = GO_CPU;

	/* poll for status change */
	while (((*(addr + TETRIS_PD_CPU_N_PDSTAT) >> PD_ACTUAL_STATE_SHIFT)
	       & PD_OFF))
		;

	return PSCI_SUCCESS;
}

int skern_poweroff_cpu(int cpu_id)
{
	unsigned int volatile *addr;
	unsigned int val;

	if (debug) {
		skern_puts(">>>> skern_poweroff_cpu >>>>");
		skern_putui(cpu_id);
		skern_puts("\r\n");
	}

	if (cpu_id >= ARM_CLUSTER_NUM_CPUS)
		return PSCI_INV_PARAM;

	skernel_cpu_data.cpu[cpu_id].kern_ep = 0;
	addr = (unsigned int *)(skernel_cpu_data.tetris_dpsc_base +
					TETRIS_PD_DPSC_OFFSET);

	/* power off the DPSC */
	addr += skern_get_dpsc_offset(cpu_id);
	val = *(addr + TETRIS_PD_CPU_N_PDSTAT);

	/* If we are already off, just return */
	if (((val & PD_ACTUAL_STATE_MASK) >> PD_ACTUAL_STATE_SHIFT) == PD_OFF)
		return PSCI_INV_PARAM;

	*(addr + TETRIS_PD_CPU_N_PTCTL) = PD_OFF;
	*(addr + TETRIS_PD_CPU_N_PTCMD) = GO_CPU;

	if (cpu_id != chip_get_arm_num()) {
		/*
		 * CPU A is powering off CPU B
		 * To complete the operation CPU B has to be either in
		 * OFF wait for interrupt/event state orr call wfi/wfe
		 * instruction. CPU cannot just wait here for completion.
		 * So, we just exit from here
		 */
		return PSCI_SUCCESS;
	}

	/* CPU wants to kill itself */

	disable_cache();
	flush_cache();

	asm volatile (
	/* disable SMP bit */
		"mrc	p15, 0, r8, c1, c0, 1\n"
		"bic	r8, r8, #(1 << 6)\n"
		"mcr	p15, 0, r8, c1, c0, 1\n"
		"isb\n"
		"dsb\n"
		:::"cc", "r8", "memory");

	/*
	 * for all CPUs except CPU0 park here.
	 * CPU 0 is a special case. Before calling WFI it has to disable
	 * tetris clocks and power off tetris power domain
	 */
	if (cpu_id != 0) {
		asm volatile ("wfi\n");
		return PSCI_INTERNAL_FAILURE;
	}

	return PSCI_SUCCESS;
}
